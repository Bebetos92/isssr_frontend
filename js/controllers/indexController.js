app.controller("indexController", function ($scope, UserService, ResourceService) {
    $scope.utenti = [UserService.getTM(), UserService.getMC(), UserService.getML()];


    $scope.optionsUser = [{label: UserService.getTM(), value: UserService.getTM(), id: UserService.getTM()},
        {label: UserService.getMC(), value: UserService.getMC(), id: UserService.getMC()},
        {label: UserService.getML(), value: UserService.getML(), id: UserService.getML()}];

    $scope.selectValue = $scope.optionsUser[0];

    $scope.initAll = function () {
        var user = $scope.selectValue.label;
        UserService.setUser(user);
        $scope.Show_ML = (!angular.equals(user, UserService.getML()));
        $scope.Show_TM = angular.equals(user, UserService.getTM());
        $scope.Show_MC = !angular.equals(user, UserService.getMC());
    };

    $scope.findUtente = function () {
        var user = $scope.selectValue.label;
        UserService.setUser(user);
        $scope.Show_ML = (!angular.equals(user, UserService.getML()));
        $scope.Show_TM = angular.equals(user, UserService.getTM());
        $scope.Show_MC = !angular.equals(user, UserService.getMC());

        //$location.path('/index.html', false);
    };

    $scope.update = function () {
        if (confirm("SEI SICURO DI VOLER AGGIORNARE IL DATABASE?\nQuesta operazione può richiedere molto tempo.")) {
            return ResourceService.updateDB({
                update: "update"
            }).then(
                function () {
                    alert("Aggiornamento dati avvenuto con successo");
                    $window.location.href = '#!/';
                },
                function () {
                    alert("ERRORE AGGIORNAMENTO DATI")
                });
        }
    }

});


