app.controller('ctrlRep', function ($scope, ResourceService, $window, UserService) {

    $scope.optionsReports = [];
    $scope.noRep = false;
    $scope.user = "";


    $scope.loadReport = function () {
        var u =  UserService.getUser();
        if (angular.equals(u,UserService.getTM()))
            $scope.user = "reportsTM";
        else if(angular.equals(u,UserService.getMC()))
            $scope.user = "reportsMC";
        else
            $scope.user = "reportsML";
            return ResourceService.getReports({
                utente: UserService.getUser()
            }).then(
                function (data) {
                    $scope.optionsReports = data;
                    if (data.length === 0)
                        $scope.noRep = true;
                },
                function () {
                    $scope.optionsReports = [];
                });
    };


    $scope.deleteRep = function (param, id) {

        if (confirm("Procedere con l'eliminazione del report?")) {
            return ResourceService.deleteReport({
                name: param,
                utente: UserService.getUser()
            }).then(
                function (data) {
                    $scope.optionsReports.splice(id, 1);
                    alert("report " + "eliminato con successo");
                },
                function (err) {
                    alert("ERRORE NELLA CANCELLAZIONE DEL REPORT");
                });
        }

    };


});