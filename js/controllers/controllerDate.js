app.controller('dateCtrl', function ($rootScope, $scope, ServiceCalculate, $sce, UserService, ResourceService) {

    var date1 = 0;
    $scope.htm = "";
    $scope.pdf = false;
    $scope.pdfurl = "";
    $scope.whait = false;

    $scope.$watch('$root.dataGraph', function () {
        $scope.whait = false;
        var value = $rootScope.dataGraph;
        if (angular.equals(value, ""))
            return;
        $scope.htm = "";
        if (value === undefined)
            return;
        var data = value.html;
        if (data.length < 600) {
            alert("Il periodo selezionato non contiene abbastanza dati per effettuare una statistica");
            return;
        }
        $scope.pdf = true;
        $scope.pdfurl = "http://localhost:8080/report/download=" + value.fileName.replace("src/main/resources/", "");

        while (data.includes('ont-size: 10px;'))
            data = data.replace('ont-size: 10px;', "");
        while (data.includes(' color: #000000;'))
            data = data.replace(' color: #000000;', "");


        while (data.includes('<td width="50%">&nbsp;</td>'))
            data = data.replace('<td width="50%">&nbsp;</td>', "");

        while (data.includes('#00FFFF'))
            data = data.replace('#00FFFF', "#969696; color: white");


        while (data.includes('border: 1px solid #000000;'))
            data = data.replace('border: 1px solid #000000;', "");

        while (data.includes(' text-align: right;'))
            data = data.replace(' text-align: right;', "");

        while (data.includes('#C0C0C0'))
            data = data.replace('#C0C0C0', "#24c18e; color:white; padding-left: 0.2rem");
        var user = UserService.getUser();
        while (data.includes('img src="re'))
            data = data.replace('img src="re', 'img class="imag" src="http://localhost:8080/report/'+user+'/img=re');

        while (data.includes("<br/>"))
            data = data.replace("<br/>", "");

        while (data.includes('td colspan="4" style="padd'))
            data = data.replace('td colspan="4" style="padd', 'td colspan="4" style=" background-color: #d0e8f9" padd');


        data = data.replace('td colspan="4" style=" back', 'td colspan="4" style=" visibility: hidden; back');

        dati = data;
        $scope.htm = $sce.trustAsHtml(dati);
        $rootScope.dataGraph = "";

    });

    /*Funzione per avviare il calco del grafico*/
    $scope.calculateGraph = function () {
        $scope.whait = true;
        $scope.htm = "";
        $scope.pdf = false;
        ServiceCalculate.calculate();
    };

    $scope.optionsInterval = [{label: "Mensile", value: "1", id: "Mensile"},
        {label: "Bimensile", value: "2", id: "Bimensile"},
        {label: "Trimestrale", value: "3", id: "Trimestrale"},
        {label: "Semestrale", value: "6", id: "Semestrale"},
        {label: "Annuale", value: "12", id: "Annuale"}];


    /*Funzione per l'intervallo di tempo*/
    $scope.selectedInterv = function (item) {
        ServiceCalculate.setInterval(item.value);
    };


    //BLOCCO GESTIONE DATEPICKER

    /*Inizzializzazione data massima*/
    $scope.dateOn = function () {
        var now = new Date();
        $scope.maxDate = (now.getMonth() + 1) + "/" + now.getDate() + "/" + now.getFullYear();
    };

    /*Controllore della data di inizio*/
    $scope.$watch('date1', function (value) {
        if (!(angular.equals(value, "") || value === null || value === undefined)) {

            var value2 = value.toString().replace(" /", "");
            value2 = value2.replace(" /", "");
            var u = value2.split(" ");

            /// mese/giorno/anno
            value2 = u[1] + "/" + u[0] + "/" + u[2];
            $scope.minDate = value2;
            var first = new Date(value2);
            ServiceCalculate.setDateStart(first);
            var second = new Date(date1);
            if (first > second) {
                date1 = value2;
                $scope.dateSet = value2;
            }
        }
    });

    /*Controllore della data di fine*/
    $scope.$watch('date2', function (value) {
        if (!(angular.equals(value, "") || value === null || value === undefined)) {


            var value2 = value.toString().replace(" /", "");
            value2 = value2.replace(" /", "");
            var u = value2.split(" ");

            /// mese/giorno/anno
            value2 = u[1] + "/" + u[0] + "/" + u[2];
            ServiceCalculate.setDateEnd(new Date(value2));
            date1 = value2;

        }
    });

});