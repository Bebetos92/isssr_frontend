app.controller('indexCtrl', function ($scope, ResourceService, $window) {

    $scope.textIndex = "";
    $scope.optionIndex = [];
    $scope.listOperation = [];
    $scope.optionVariable = [];
    $scope.parenthesis = 0;
    $scope.variable = "";
    $scope.sem = false;
    $scope.whait = false;


    $scope.collectIndex = function (param) {
        ResourceService.getIndex({
            category: param
        }).then(
            function (data) {
                $scope.optionIndex = data;
            },
            function () {
                $scope.optionIndex = [];
            });
    };

    $scope.addText = function (value) {
        if (angular.equals(value, " ( "))
            $scope.parenthesis++;
        if (angular.equals(value, " ) "))
            $scope.parenthesis--;
        $scope.textIndex += value;
        $scope.listOperation.push(value);
    };

    $scope.clean = function () {
        $scope.parenthesis = 0;
        $scope.textIndex = "";
    };

    $scope.back = function () {
        var t = $scope.listOperation.pop();
        if (angular.equals(t, " ( "))
            $scope.parenthesis--;
        if (angular.equals(t, " ) "))
            $scope.parenthesis++;
        for (var i = 0; i < t.length; i++) {
            $scope.textIndex = $scope.textIndex.slice(0, -1);
        }
    };


    $scope.$watch('semantic', function (value) {
        $scope.parenthesis = 0;
        $scope.textIndex = "";
        if (!(angular.equals(value, "") || value === null || value === undefined)) {
            $scope.sem = true;
            $scope.variable = value;
            ResourceService.getVariable({
                str: value
            }).then(
                function (data) {
                    $scope.optionVariable = data;
                },
                function () {
                    $scope.optionVariable = [];
                });
            $scope.collectIndex(value);
        }
    });

    $scope.save = function () {
        if (!angular.equals($scope.parenthesis, 0)) {
            alert("Errore inserimento indice, controllare le parentesi");
            return;
        }
        if ($scope.indName === undefined || $scope.indName === null || angular.equals($scope.indName, "")) {
            alert("Inserire un nome per il nuovo indice");
            return;
        }
        if ($scope.indDescr === undefined || $scope.indDescr === null || angular.equals($scope.indDescr, "")) {
            alert("Inserire una descrizione per il nuovo indice");
            return;
        }
        if (angular.equals($scope.textIndex, "")) {
            alert("Inserire una formula per il nuovo indice");
            return;
        }
        $scope.whait = true;
        ResourceService.saveIndex({
            nameIndex: $scope.indName,
            category: $scope.semantic,
            description: $scope.indDescr,
            calculate: $scope.textIndex
        }).then(
            function () {
                alert("Indice aggiunto con successo");
                $window.location.href = '#!/';
                $scope.whait = false;
            },
            function () {
                $scope.whait = false;
                alert("Errore nel salvataggio del nuovo indice, controllare la formula!");
            });

    }
});
