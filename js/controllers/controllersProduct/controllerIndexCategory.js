app.controller('ctrlCategory', function ($scope, ResourceService, ServiceCalculate, UserService) {


    $scope.optionIndex = [];
    $scope.optionsCat = [];
    $scope.optionSalePoint = [];
    $scope.seldisable = false;
    $scope.identityVal = 0;
    $scope.product = [];
    $scope.sp = true;
    var rolesel = false;
    var spl = "";


    $scope.documOn = function (value) {
        if (angular.equals(value, "Cat")) {
            ServiceCalculate.setPage("Cat");
        }
    };

    /*Funzione che aggiunge un elemento category con id */
    $scope.addNewCategory = function () {
        if ($scope.product.length < 4) {
            $scope.identityVal++;
            $scope.product.push({'id': 'category' + $scope.identityVal});
        }
    };

    /*Funzione che rimuove dall'array l'elemento passato*/
    $scope.removeCategory = function (value) {
        ServiceCalculate.removeCategor(value + 1);
        $scope.product.splice(value, 1);
    };

    $scope.$watch('typeobj', function (value) {
        $scope.optionsCat = [];
        if (!(angular.equals(value, "") || value === null || value === undefined)) {
            if (angular.equals(value, "0"))
                rolesel = false;
            else if (angular.equals(value, "1"))
                rolesel = true;
            var l=$scope.product.length;
            for (var i = -1; i < l; i++) {
                $scope.removeCategory(0);
            }
        }
    });

    /*Funzione che si occupa della selezione di una categoria*/
    $scope.selectedCategory = function (item, model, id) {
        ServiceCalculate.addCategory(item, id + 1);
    };

    /*Funzione che si occupa della selezione di un SP*/
    $scope.selectedSP = function (item) {
        ServiceCalculate.addSalePoint(item, 0);
        $scope.seldisable = true;
        $scope.sp = false;
        spl = item;
    };

    /*Funzione che si occupa della selezione di un index*/
    $scope.selectedIndex = function (item) {
        ServiceCalculate.setIndex(item);
    };

    $scope.refreshSalePoint = function (input) {
        if (!angular.isDefined(input) || input === null) {
            $scope.seldisable = false;
            return [];
        }
        if (input.length === 0) {
            return ResourceService.getPos({
                str: input
            }).then(
                function (data) {
                    $scope.optionSalePoint = data;
                },
                function () {
                    $scope.optionSalePoint = [];
                });
        }
    };

    $scope.refreshIndexCat = function (input) {
        var user = UserService.getUser();
        if (!angular.isDefined(input) || input === null) return [];
        if (angular.equals(user, UserService.getMC()))
            refreshIndex(input, "product");
        else {
            if (rolesel) {
                refreshIndex(input, "food");
            }
            else {
                refreshIndex(input, "product");
            }
        }
    };

    /*Funzione che crea dinamicamente la lista degli indici*/
    var refreshIndex = function (input, type) {
        if (!angular.isDefined(input) || input === null) return [];
        if (input.length === 0) {
            ResourceService.getIndex({
                category: type
            }).then(
                function (data) {
                    $scope.optionIndex = data;
                },
                function () {
                    $scope.optionIndex = [];
                });
        }
    };

    $scope.refreshCategoryPrFo = function (input) {
        if (!angular.isDefined(input) || input === null) {
            $scope.optionsCat = [];
            return [];
        }
        var user = UserService.getUser();
        if (angular.equals(user, UserService.getTM())) {
            if (rolesel) {
                if (angular.equals(spl.name, UserService.getMagCen())) {
                    alert("Il magazzino centrale non contiene piatti");
                    return;
                }
                refreshCategoryFood(input, user, spl.name);
            }
            else {
                refreshCategoryProduct(input, user);
            }
        }
        else if (angular.equals(user, UserService.getML())) {
            if (rolesel) {
                if (angular.equals(spl.name, UserService.getMagCen())) {
                    alert("Il magazzino centrale non contiene piatti");
                    return;
                }
                refreshCategoryFood(input, user, UserService.getRmCn());
            }
            else {
                refreshCategoryProduct(input, user);
            }
        }
        else {
            refreshCategoryProduct(input, user);
        }
    };

    var refreshCategoryFood = function (input, user, location) {
        if(!angular.isDefined(location))
        {
            $scope.optionsCat = [];
            return;
        }
        ResourceService.getCategoryFood({
            str: input,
            utente: user,
            location: location
        }).then(
            function (data) {
                $scope.optionsCat = data;
            },
            function () {
                $scope.optionsCat = [];
            });
    };

    var refreshCategoryProduct = function (input, user) {
        ResourceService.getCategory({
            str: input,
            utente: user
        }).then(
            function (data) {
                $scope.optionsCat = data;
            },
            function () {
                $scope.optionsCat = [];
            });
    };

});