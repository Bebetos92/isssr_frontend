app.controller('ctrlProduct', function ($scope, ResourceService, ServiceCalculate, UserService) {

    $scope.optionsProd = [];
    $scope.optionsCat = [];
    $scope.optionIndex = [];
    $scope.optionSalePoint = [];
    $scope.product = [];
    $scope.seldisable = [];
    $scope.identityVal = 0;
    var selectedItemCategory = [];


    $scope.documOn = function (value) {
        if (angular.equals(value, "Prod")) {
            $scope.seldisable[0] = false;
            ServiceCalculate.setPage("Prod");
        }
    };

    /*Funzione che aggiunge un elemento product con id */
    $scope.addNewProduct = function () {
        if ($scope.product.length < 4) {
            $scope.seldisable.push(false);
            $scope.identityVal++;
            $scope.product.push({'id': 'product' + $scope.identityVal});

        }
    };

    /*Funzione che rimuove dall'array l'elemento passato*/
    $scope.removeProduct = function (value) {
        ServiceCalculate.removeProduc(value + 1);
        $scope.product.splice(value, 1);
    };

    /*Funzione che si occupa della selezione di una prodotto*/
    $scope.selectedProduct = function (item, model, id) {
        ServiceCalculate.addProduct(item, id + 1);
    };

    /*Funzione che si occupa della selezione di un index*/
    $scope.selectedIndex = function (item) {
        ServiceCalculate.setIndex(item);
    };

    /*Funzione che si occupa della selezione di una categoria*/
    $scope.selectedCategory = function (item, model, id,val) {
        ServiceCalculate.addCategory(item, id + 1);
        selectedItemCategory[val] = item;
        $scope.seldisable[val] = true;
    };

    /*Funzione che si occupa della selezione di un SP*/
    $scope.selectedSP = function (item) {
        ServiceCalculate.addSalePoint(item, 0);

    };

    /*Funzione che crea dinamicamente la lista dei prodotti cercati*/
    $scope.refreshProduct = function (input, id) {
        id = id.id.replace("product", "");
        if (!angular.isDefined(input) || input === null) {
            $scope.optionsProd = [];
            return [];
        }
        if (input.length < 1) {
            $scope.optionsProd = [];
            return [];
        }
        if (input.length === 1) {
            return ResourceService.getPartialProducts({
                category: selectedItemCategory[id].name,
                threechar: input
            }).then(
                function (data) {
                    $scope.optionsProd = data;
                },
                function () {
                    $scope.optionsProd = [];
                });
        }
    };

    /*Funzione che crea dinamicamente la lista delle categorie cercate*/
    $scope.refreshCategory = function (input) {
        if (!angular.isDefined(input) || input === null) {
            $scope.optionsCat = [];
            return [];
        }
        if (input.length < 1) {
            $scope.optionsCat = [];
            return [];
        }
        if (input.length === 1) {
            $scope.optionsProd = [];
            ResourceService.getCategory({
                str: input,
                utente: UserService.getUser()
            }).then(
                function (data) {
                    $scope.optionsCat = data;
                },
                function () {
                    $scope.optionsCat = [];
                });
        }
    };

    /*Funzione che crea dinamicamente la lista degli indici*/
    $scope.refreshIndexPro = function (input) {
        if (!angular.isDefined(input) || input === null) return [];
        if (input.length === 0) {
            ResourceService.getIndex({
                category: "product"
            }).then(
                function (data) {
                    $scope.optionIndex = data;
                },
                function () {
                    $scope.optionIndex = [];
                });
        }
    };

    /*Funzione che crea dinamicamente la lista dei SP*/
    $scope.refreshSalePoint = function (input) {
        if (!angular.isDefined(input) || input === null) {
            $scope.seldisable[0] = false;
            return [];
        }
        if (input.length === 0) {
            return ResourceService.getPos({
                str: input
            }).then(
                function (data) {
                    $scope.optionSalePoint = data;
                },
                function () {
                    $scope.optionSalePoint = [];
                });
        }
    };

});