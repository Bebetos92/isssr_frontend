app.controller('ctrlFood', function ($scope, ResourceService, ServiceCalculate, UserService) {

    $scope.optionIndex = [];
    $scope.optionsProd = [];
    $scope.optionSalePoint = [];
    $scope.product = [];
    $scope.seldisable = false;
    $scope.identityVal = 0;
    var selectedItemSP = "";


    $scope.documOn = function (value) {
        if (angular.equals(value, "Food")) {
            ServiceCalculate.setPage("Food");
        }

    };

    /*Funzione che aggiunge un elemento product con id */
    $scope.addNewProduct = function () {
        if ($scope.product.length < 4) {
            $scope.identityVal++;
            $scope.product.push({'id': 'food' + $scope.identityVal});

        }
    };

    /*Funzione che rimuove dall'array l'elemento passato*/
    $scope.removeProduct = function (value) {
        ServiceCalculate.removeProduc(value + 1);
        $scope.product.splice(value, 1);
    };

    /*Funzione che si occupa della selezione di una prodotto*/
    $scope.selectedProduct = function (item, model, id) {
        ServiceCalculate.addProduct(item, id + 1);
    };

    /*Funzione che si occupa della selezione di un SP*/
    $scope.selectedSP = function (item) {
        ServiceCalculate.addSalePoint(item, 0);
        selectedItemSP = item.name;
        $scope.seldisable = true;
    };

    /*Funzione che si occupa della selezione di un index*/
    $scope.selectedIndex = function (item) {
        ServiceCalculate.setIndex(item);
    };

    /*Funzione che crea dinamicamente la lista dei SP*/
    $scope.refreshSalePoint = function (input) {
        if (!angular.isDefined(input) || input === null) {
            $scope.seldisable = false;
            return [];
        }
        if (input.length === 0) {
            return ResourceService.getPos({
                str: input
            }).then(
                function (data) {
                    $scope.optionSalePoint = [];
                    for (var i = 0; i < data.length; i++) {
                        if (!angular.equals(data[i].name, UserService.getMagCen()))
                            $scope.optionSalePoint.push(data[i]);
                    }
                },
                function () {
                    $scope.optionSalePoint = [];
                });
        }
    };

    /*Funzione che crea dinamicamente la lista dei prodotti cercati*/
    $scope.refreshFood = function (input) {
        if (angular.equals(UserService.getUser(), UserService.getML())) {
            selectedItemSP = UserService.getRmCn();       // usato perche non si ha a disposizione un servizio di autenticazione
        }
        if (!angular.isDefined(input) || input === null) {
            $scope.optionsProd = [];
            return [];
        }
        if (input.length === 0 && selectedItemSP !== null && selectedItemSP !== undefined) {
            return ResourceService.getPartialFood({
                location: selectedItemSP
            }).then(
                function (data) {
                    $scope.optionsProd = data;
                },
                function () {
                    $scope.optionsProd = [];
                });
        }
    };

    /*Funzione che crea dinamicamente la lista degli indici*/
    $scope.refreshIndexFood = function (input) {
        if (!angular.isDefined(input) || input === null) return [];
        if (input.length === 0) {
            ResourceService.getIndex({
                category: "food"
            }).then(
                function (data) {
                    $scope.optionIndex = data;
                },
                function () {
                    $scope.optionIndex = [];
                });
        }
    };


});