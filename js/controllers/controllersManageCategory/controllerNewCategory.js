app.controller('ctrlNewCategory', function ($scope, ResourceService, ServiceCalculate, UserService,$window) {

    $scope.optionsCat = [];
    $scope.optionsProd = [];
    $scope.optionSalePoint = [];
    $scope.productSel = [];
    $scope.categorySel = [];
    $scope.selectedItemSP = "";
    $scope.rolesel = false;
    $scope.sp = false;




    $scope.$watch('typeobj', function (value) {
        if (!(angular.equals(value, "") || value === null || value === undefined)) {
            if (angular.equals(value, "0"))
                $scope.rolesel = false;
            else if (angular.equals(value, "1")) {
                $scope.rolesel = true;
                $scope.sp = false;
            }
            $scope.optionsCat = [];
        }
    });

    /*Funzione che si occupa della selezione di un SP*/
    $scope.selectedSP = function (item) {
        $scope.selectedItemSP = item.name;
        $scope.sp = true;

    };

    /*Funzione che crea dinamicamente la lista dei SP*/
    $scope.refreshSalePoint = function (input) {
        $scope.productSel = [];
        $scope.categorySel = [];
        $scope.optionsCat = [];
        $scope.optionsProd = [];
        if (!angular.isDefined(input) || input === null) {
            return [];
        }
        if (input.length === 0) {
            return ResourceService.getPos({
                str: input
            }).then(function (data) {
                    if ($scope.rolesel) {
                        $scope.optionSalePoint = [];
                        for (var i = 0; i < data.length; i++) {
                            if (!angular.equals(data[i].name, UserService.getMagCen()))
                                $scope.optionSalePoint.push(data[i]);
                        }
                    }
                    else
                        $scope.optionSalePoint = data;
                },
                function (err) {
                    $scope.optionSalePoint = [];
                });
        }
    };

    $scope.refreshProd = function (input) {
        if (!angular.isDefined(input) || input === null) return [];
        if (input.length < 1) {
            $scope.optionsProd = [];
            return [];
        }
        if (input.length === 1) {
            if ($scope.rolesel) {
                if (angular.equals(UserService.getUser(), UserService.getML()))
                    $scope.selectedItemSP = UserService.getRmCn();
                ResourceService.getPartialFood({
                    location: $scope.selectedItemSP
                }).then(
                    function (data) {
                        $scope.optionsProd = data;
                    },
                    function (err) {
                        $scope.optionsProd = [];
                    });
            }
            else {
                ResourceService.getAllProduct({
                    fourchar: input
                }).then(
                    function (data) {
                        $scope.optionsProd = data;
                    },
                    function (err) {
                        $scope.optionsProd = [];
                    });
            }
        }
    };

    $scope.refreshCategory = function (input) {
        if (!angular.isDefined(input) || input === null) return [];
        if (input.length < 1) {
            $scope.optionsCat = [];
            return [];
        }
        if (input.length === 1) {
            var user = UserService.getUser();
            if ($scope.rolesel) {
                if (angular.equals(user, UserService.getML()))
                    $scope.selectedItemSP = UserService.getRmCn();
                ResourceService.getCategoryFood({
                    str: input,
                    utente: user,
                    location: $scope.selectedItemSP
                }).then(
                    function (data) {
                        $scope.optionsCat = data;
                    },
                    function (err) {
                        $scope.optionsCat = [];
                    });
            }
            else {
                ResourceService.getCategory({
                    utente: user,
                    str: input
                }).then(
                    function (data) {
                        $scope.optionsCat = data;
                    },
                    function (err) {
                        $scope.optionsCat = [];
                    });
            }
        }
    };

    $scope.saveCat = function () {

        var p;
        var prod = [];
        var cat = [];
        var pro = false;
        var user = UserService.getUser();

        if ($scope.catName === undefined || $scope.catName === null || angular.equals($scope.catName, "")) {
            alert("Inserire un nome per la nuova categoria");
            return;
        }
        if ($scope.catDescr === undefined || $scope.catDescr === null || angular.equals($scope.catDescr, "")) {
            alert("Inserire una descrizione per la nuova categoria");
            return;
        }

        if ($scope.productSel.selected !== undefined && $scope.productSel.selected !== null)
            for (p = 0; p < $scope.productSel.selected.length; p++)
                if (!angular.equals(user, UserService.getMC())) {
                    if ($scope.rolesel)
                        prod.push($scope.productSel.selected[p].nome);
                    else
                        prod.push($scope.productSel.selected[p].description);
                }
                else
                    pro = true;

        if ($scope.categorySel.selected !== undefined && $scope.categorySel.selected !== null)
            for (p = 0; p < $scope.categorySel.selected.length; p++)
                cat.push($scope.categorySel.selected[p].name);
        else {
            if (pro) {
                alert("Inserire almeno un oggetto o una categoria");
                return;
            }
        }

        if (angular.equals(user, UserService.getML())) {
            $scope.selectedItemSP = UserService.getRmCn();
        }

        if (!angular.equals(user, UserService.getMC())) {
            if ($scope.rolesel) {
                ResourceService.addCatFood({
                    "name": $scope.catName,
                    "description": $scope.catDescr,
                    "utente": user,
                    "categorieFood": cat,
                    "food": prod,
                    "location" : $scope.selectedItemSP
                }).then(function () {
                        alert("La nuova categoria è stata aggiunta con successo");
                        $window.location.href = '#!/manageCategory';
                    }, function () {
                        alert("ERRORE NELLA CREAZIONE DELLA NUOVA CATEGORIA");
                    }
                );
            } else {
                ResourceService.addCat({
                    "name": $scope.catName,
                    "description": $scope.catDescr,
                    "prodotti": prod,
                    "categorie": cat,
                    "utente": user
                }).then(function () {
                        alert("La nuova categoria è stata aggiunta con successo");
                        $window.location.href = '#!/manageCategory';
                    }, function () {
                        alert("ERRORE NELLA CREAZIONE DELLA NUOVA CATEGORIA");
                    }
                );
            }
        }
        else {
            ResourceService.addCat({
                "name": $scope.catName,
                "description": $scope.catDescr,
                "prodotti": prod,
                "categorie": cat,
                "utente": user
            }).then(function () {
                    alert("La nuova categoria è stata aggiunta con successo");
                    $window.location.href = '#!/manageCategory';
                }, function () {
                    alert("ERRORE NELLA CREAZIONE DELLA NUOVA CATEGORIA");
                }
            );

        }
    };



});