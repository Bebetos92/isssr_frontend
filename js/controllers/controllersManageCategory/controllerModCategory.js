app.controller('ctrlModCategory', function ($rootScope,$scope, ResourceService, ServiceCalculate, UserService,$window) {

    $scope.optionsCat = [];
    $scope.optionsProd = [];
    $scope.optionSalePoint = [];
    $scope.productSel = [];
    $scope.categorySel = [];



    $scope.$watch('optionsProduct', function () {
        $scope.productSel.selected = $rootScope.optionsProduct;
    });





    $scope.refreshProd = function (input) {
        if (!angular.isDefined(input) || input === null) return [];
        if (input.length < 1) {
            $scope.optionsProd = [];
            return [];
        }
        if (input.length === 1) {
            if ($rootScope.isFood) {
                if (angular.equals(UserService.getUser(), UserService.getML()))
                    $scope.selectedItemSP = UserService.getRmCn();
                ResourceService.getPartialFood({
                    location: $scope.selectedItemSP
                }).then(
                    function (data) {
                        $scope.optionsProd = data;
                    },
                    function (err) {
                        $scope.optionsProd = [];
                    });
            }
            else {
                ResourceService.getAllProduct({
                    fourchar: input
                }).then(
                    function (data) {
                        $scope.optionsProd = data;
                    },
                    function (err) {
                        $scope.optionsProd = [];
                    });
            }
        }
    };

    $scope.refreshCategory = function (input) {
        if (!angular.isDefined(input) || input === null) return [];
        if (input.length < 1) {
            $scope.optionsCat = [];
            return [];
        }
        if (input.length === 1) {
            var user = UserService.getUser();
            if ($rootScope.isFood) {
                if (angular.equals(user, UserService.getML()))
                    $scope.selectedItemSP = UserService.getRmCn();
                ResourceService.getCategoryFood({
                    str: input,
                    utente: user,
                    location: $scope.selectedItemSP
                }).then(
                    function (data) {
                        $scope.optionsCat = data;
                    },
                    function (err) {
                        $scope.optionsCat = [];
                    });
            }
            else {
                ResourceService.getCategory({
                    utente: user,
                    str: input
                }).then(
                    function (data) {
                        $scope.optionsCat = data;
                    },
                    function (err) {
                        $scope.optionsCat = [];
                    });
            }
        }
    };

    $scope.saveCat = function () {

        var p;
        var prod = [];
        var cat = [];
        var pro = false;
        var user = UserService.getUser();

        if ($scope.catName === undefined || $scope.catName === null || angular.equals($scope.catName, "")) {
            alert("Inserire un nome per la nuova categoria");
            return;
        }
        if ($scope.catDescr === undefined || $scope.catDescr === null || angular.equals($scope.catDescr, "")) {
            alert("Inserire una descrizione per la nuova categoria");
            return;
        }

        if ($scope.productSel.selected !== undefined && $scope.productSel.selected !== null)
            for (p = 0; p < $scope.productSel.selected.length; p++)
                if (angular.equals(user, UserService.getTM())) {
                    if ($rootScope.isFood)
                        prod.push($scope.productSel.selected[p].nome);
                    else
                        prod.push($scope.productSel.selected[p].description);
                }
                else
                    pro = true;

        if ($scope.categorySel.selected !== undefined && $scope.categorySel.selected !== null)
            for (p = 0; p < $scope.categorySel.selected.length; p++)
                cat.push($scope.categorySel.selected[p].name);
        else {
            if (pro) {
                alert("Inserire almeno un oggetto o una categoria");
                return;
            }
        }

        if (angular.equals(user, UserService.getTM())) {
            if ($rootScope.isFood) {
                ResourceService.addCatFood({
                    "name": $scope.catName,
                    "description": $scope.catDescr,
                    "utente": user,
                    "categorieFood": cat,
                    "food": prod,
                    "location" : $scope.selectedItemSP
                }).then(function () {
                        alert("La categoria è stata modificata con successo");
                        $window.location.href = '#!/manageCategory';
                    }, function () {
                        alert("ERRORE NELLA MODIFICA DELLA CATEGORIA");
                    }
                );
            } else {
                ResourceService.addCat({
                    "name": $scope.catName,
                    "description": $scope.catDescr,
                    "prodotti": prod,
                    "categorie": cat,
                    "utente": user
                }).then(function () {
                        alert("La categoria è stata modificata con successo");
                        $window.location.href = '#!/manageCategory';
                    }, function () {
                        alert("ERRORE NELLA MODIFICA DELLA CATEGORIA");
                    }
                );
            }
        }
    };



});