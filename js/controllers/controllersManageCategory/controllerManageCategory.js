app.controller('ctrlManCategory', function ($rootScope, $scope, ResourceService, $window, UserService) {


    $rootScope.optionsProduct = [];
    $scope.optionsCat = [];
    $scope.optionsCatFood = [];
    $scope.category = "";
    $scope.catId = "";
    $scope.yes = false;
    $scope.yes2 = false;
    $rootScope.cat="";
    $rootScope.selectedItemSP = "";
    $rootScope.productSel="";
    $rootScope.isProduct = false;
    $rootScope.isFood = false;
    $rootScope.rolesel = 0;

    var location = [];
    var product = 0;


    $scope.isStd = function (param) {
    if (param.description === "Categoria standard"){
        return false;
    }
    else
        return true;
    };


    $scope.loadCat = function () {
        $scope.optionsCat = [];
        var user = UserService.getUser();


        ResourceService.getCategory({
            utente: user,
            str: ""
        }).then(
            function (data) {
                $scope.optionsCat = $scope.optionsCat.concat(data);
                product = $scope.optionsCat.length;
                if (angular.equals(user, UserService.getTM())) {
                    ResourceService.getPos({
                        str: ""
                    }).then(function (data) {
                            for (var i = 0; i < data.length; i++) {
                                if (!angular.equals(data[i].name, UserService.getMagCen()))
                                    location.push(data[i].name);
                            }
                            for (i = 0; i < location.length; i++)
                                ResourceService.getCategoryFood({
                                    str: "",
                                    utente: user,
                                    location: location[i]
                                }).then(
                                    function (data) {
                                        $scope.optionsCat = $scope.optionsCat.concat(data);
                                    },
                                    function () {
                                        $scope.optionsCat = [];
                                    });

                        },
                        function () {
                            location = [];
                        });
                }
                if (angular.equals(user, UserService.getML())) {

                    ResourceService.getCategoryFood({
                        str: "",
                        utente: user,
                        location: UserService.getRmCn()
                    }).then(
                        function (data) {
                            $scope.optionsCat = $scope.optionsCat.concat(data);
                        },
                        function () {
                            $scope.optionsCat = [];
                        });
                }
            },
            function () {
                $scope.optionsCat = [];
            });
    };


    $scope.focusCat = function (param, id) {
        $rootScope.optionsProduct = [];
        $scope.category = param.name;
        if (id < product) {
            ResourceService.getPartialProducts({
                category: $scope.category,
                threechar: ""
            }).then(
                function (data) {
                    $rootScope.optionsProduct = data;
                },
                function () {
                    $rootScope.optionsProduct = [];
                });
        }
        else {
            var loc;
            if (angular.equals(UserService.getUser(), UserService.getML())) {
                loc = UserService.getRmCn();       // usato perche non si ha a disposizione un servizio di autenticazione
            }
            else
                loc = param.location;
            ResourceService.getFoodCategory({
                category: $scope.category,
                location: loc
            }).then(
                function (data) {

                    data = data.map(function (obj) {
                        return angular.extend(obj, {'type': 'food'});
                    });
                    $rootScope.optionsProduct = data;
                },
                function () {
                    $rootScope.optionsProduct = [];
                });
        }
        $scope.yes = true;
    };
    $scope.focusModify = function (param, id) {
        $rootScope.optionsProduct = [];
        $scope.category = param.name;
        if (id < product) {
            $rootScope.isProduct = true;
            $rootScope.isFood = false;
            ResourceService.getPartialProducts({
                category: $scope.category,
                threechar: ""
            }).then(
                function (data) {
                    $rootScope.optionsProduct = data;
                },
                function () {
                    $rootScope.optionsProduct = [];
                });
        }
        else {
            $rootScope.isFood = true;
            $rootScope.isProduct = false;
            $rootScope.typeobj = 1;
            var loc;
            if (angular.equals(UserService.getUser(), UserService.getML())) {
                loc = UserService.getRmCn();       // usato perche non si ha a disposizione un servizio di autenticazione
            }
            else
                loc = param.location;
            ResourceService.getFoodCategory({
                category: $scope.category,
                location: loc
            }).then(
                function (data) {

                    data = data.map(function (obj) {
                        return angular.extend(obj, {'type': 'food'});
                    });
                    $rootScope.optionsProduct = data;
                },
                function () {
                    $rootScope.optionsProduct = [];
                });
        }
        $scope.catName = param.name;
        $scope.catId = param.id;
        $scope.catDescr = param.description;
        $scope.catLoc = param.location;
        $rootScope.selectedItemSP = param.location;
        $scope.yes2 = true;
    };


    $scope.deleteCat = function (param, id) {
        if (confirm("Procedere con l'eliminazione della categoria?")) {

            if (id > product) {
                ResourceService.deleteCategoryFood({
                    utente: UserService.getUser(),
                    location: param.location,
                    name: param.name
                }).then(
                    function () {
                        $scope.optionsCat.splice(id, 1);

                        alert("categoria " + "eliminata con successo");
                    },
                    function () {
                        alert("ERRORE NELLA CANCELLAZIONE DELLA CATEGORIA");
                    });
            } else {
                ResourceService.deleteCategory({
                    name: param.name
                }).then(
                    function () {
                        $scope.optionsCat.splice(id, 1);
                        product--;
                        alert("categoria " + "eliminata con successo");
                    },
                    function () {
                        alert("ERRORE NELLA CANCELLAZIONE DELLA CATEGORIA");
                    });
            }
        }
    };

    $scope.hide = function () {
        $scope.yes = false;
    };
    $scope.hide2 = function () {
        $scope.yes2 = false;
    }

});