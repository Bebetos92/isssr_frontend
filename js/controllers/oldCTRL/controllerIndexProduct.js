app.controller('mySelect', function ($scope, ResourceService, ServiceCalculate, UserService) {

    $scope.optionsProd = [];
    $scope.optionsCat = [];
    $scope.optionProdCat = [];
    $scope.optionSalePoint = [];
    $scope.optionIndex = [];
    $scope.product = [];
    $scope.seldisable = [];
    $scope.salepoint = [];
    $scope.selectedItemCategory = [];
    $scope.selectedItemSP = [];
    $scope.identityVal = 0;
    $scope.identityValSP = 0;
    $scope.rolesel = false;
    $scope.sp = true;
    $scope.spl= "";


    $scope.documOn = function (value) {
        if (angular.equals(value, "Prod")) {
            $scope.seldisable[0] = false;
            ServiceCalculate.setPage("Prod");
        }
        else if (angular.equals(value, "Cat")) {
            ServiceCalculate.setPage("Cat");
        } else if (angular.equals(value, "Comp")) {
            ServiceCalculate.setPage("Comp");
            $scope.collectIndex();
        } else if (angular.equals(value, "Food")) {
            ServiceCalculate.setPage("Food");
        }

    };

    /*Funzione che aggiunge un elemento product con id */
    $scope.addNewProduct = function () {
        if ($scope.product.length < 4) {
            $scope.seldisable.push(false);
            $scope.identityVal++;
            $scope.product.push({'id': 'product' + $scope.identityVal});

        }
    };

    /*Funzione che aggiunge un elemento category con id */
    $scope.addNewCategory = function () {
        if ($scope.product.length < 4) {
            $scope.identityVal++;
            $scope.product.push({'id': 'category' + $scope.identityVal});
        }
    };

    /*Funzione che aggiunge un elemento prodcat con id */
    $scope.addNewProdCat = function () {
        if ($scope.product.length < 4) {
            $scope.identityVal++;
            $scope.product.push({'id': 'prodcat' + $scope.identityVal});
        }
    };

    /*Funzione che aggiunge un elemento sale point con id */
    $scope.addNewSalePoint = function () {
        if ($scope.salepoint.length < 4) {
            $scope.identityValSP++;
            $scope.salepoint.push({'id': 'salepoint' + $scope.identityValSP});
        }
    };

    /*Funzione che rimuove dall'array l'elemento passato*/
    $scope.removeProduct = function (value) {
        ServiceCalculate.removeProduc(value + 1);
        $scope.product.splice(value, 1);
    };

    /*Funzione che rimuove dall'array l'elemento passato*/
    $scope.removeCategory = function (value) {
        ServiceCalculate.removeCategor(value + 1);
        $scope.product.splice(value, 1);
    };

    /*Funzione che rimuove dall'array l'elemento passato*/
    $scope.removeProdCat = function (value) {
        ServiceCalculate.removeProduc(value + 1);
        ServiceCalculate.removeCategor(value + 1);
        $scope.product.splice(value, 1);
    };

    /*Funzione che rimuove all'array product l'elemento value*/
    $scope.removeSalepoint = function (value) {
        ServiceCalculate.removeSalepoin(value + 1);
        $scope.salepoint.splice(value, 1);
    };

    /*Funzione che si occupa della selezione di una prodotto*/
    $scope.selectedProduct = function (item, model, id) {
        ServiceCalculate.addProduct(item, id);
    };

    /*Funzione che si occupa della selezione di una categoria*/
    $scope.selectedCategory = function (item, model, id) {
        ServiceCalculate.addCategory(item, id);
        $scope.selectedItemCategory[id] = item;
        $scope.seldisable[id] = true;

    };


    /*Funzione che si occupa della selezione di una categoria*/
    $scope.selectedProdCat = function (item, model, id) {
        switch (item.type) {
            case "prod":
                ServiceCalculate.addProduct(item, id);
                break;
            case  "cat":
                ServiceCalculate.addCategory(item, id);
                break;
        }
    };

    /*Funzione che si occupa della selezione di un SP*/
    $scope.selectedSP = function (item, model, id) {
        ServiceCalculate.addSalePoint(item, id);

    };

    /*Funzione che si occupa della selezione di un SP*/
    $scope.selectedSP_2 = function (item, model, id) {
        ServiceCalculate.addSalePoint(item, id);
        $scope.selectedItemSP[id] = item.name;
        $scope.seldisable[id] = true;
        $scope.sp = false;
        $scope.spl = item;
    };

    /*Funzione che si occupa della selezione di un index*/
    $scope.selectedIndex = function (item) {
        ServiceCalculate.setIndex(item);
    };

    /*Funzione che crea dinamicamente la lista dei prodotti cercati*/
    $scope.refreshProduct = function (input, id) {
        id = id.id.replace("product", "");
        if (!angular.isDefined(input) || input === null) {
            $scope.optionsProd = [];
            return [];
        }
        if (input.length < 1) {
            $scope.optionsProd = [];
            return [];
        }
        if (input.length === 1) {
            return ResourceService.getPartialProducts({
                category: $scope.selectedItemCategory[id].name,
                threechar: input
            }).then(
                function (data) {
                    $scope.optionsProd = data;
                },
                function (err) {
                    $scope.optionsProd = [];
                });
        }
    };


    /*Funzione che crea dinamicamente la lista dei prodotti cercati*/
    $scope.refreshFood = function (input) {
        if (angular.equals(UserService.getUser(), UserService.getML())) {
            $scope.selectedItemSP[0] = UserService.getRmCn();       // usato perche non si ha a disposizione un servizio di autenticazione
        }
        if (!angular.isDefined(input) || input === null) {
            $scope.optionsProd = [];
            return [];
        }
        if (input.length === 0 && $scope.selectedItemSP[0] !== null && $scope.selectedItemSP[0] !== undefined) {
            return ResourceService.getPartialFood({
                location: $scope.selectedItemSP[0]
            }).then(
                function (data) {
                    $scope.optionsProd = data;
                },
                function (err) {
                    $scope.optionsProd = [];
                });
        }
    };

    /*Funzione che crea dinamicamente la lista delle categorie cercate*/
    $scope.refreshCategory = function (input) {
        if (!angular.isDefined(input) || input === null) {
            $scope.optionsCat = [];
            return [];
        }
        if (input.length < 1) {
            $scope.optionsCat = [];
            return [];
        }
        if (input.length === 1) {
            $scope.optionsProd = [];
            ResourceService.getCategory({
                str: input,
                utente: UserService.getUser()
            }).then(
                function (data) {
                    $scope.optionsCat = data;
                },
                function (err) {
                    $scope.optionsCat = [];
                });
        }
    };


    $scope.$watch('typeobj', function (value) {
        $scope.optionsPos = [];
        $scope.optionsCat = [];
        if (!(angular.equals(value, "") || value === null || value === undefined)) {
            if (angular.equals(value, "0"))
                $scope.rolesel = false;
            else if (angular.equals(value, "1"))
                $scope.rolesel = true;

        }
    });


    $scope.refreshCategoryPrFo = function (input) {
        if (!angular.isDefined(input) || input === null) {
            $scope.optionsCat = [];
            return [];
        }
        var user = UserService.getUser();
        if (angular.equals(user, UserService.getTM())) {
            if ($scope.rolesel) {
                if (angular.equals($scope.spl, "")) {
                    alert("Selezionare un punto vendita");
                    return;
                }
                if (angular.equals($scope.spl.name, UserService.getMagCen())) {
                    alert("Il magazzino centrale non contiene piatti");
                    return;
                }
                ResourceService.getCategoryFood({
                    str: input,
                    utente: user,
                    location: $scope.spl.name
                }).then(
                    function (data) {
                        $scope.optionsCat = data;
                    },
                    function (err) {
                        $scope.optionsCat = [];
                    });
            }

            else {
                ResourceService.getCategory({
                    str: input,
                    utente: user
                }).then(
                    function (data) {
                        $scope.optionsCat = data;
                    },
                    function (err) {
                        $scope.optionsCat = [];
                    });
            }
        }
        else if (angular.equals(user, UserService.getML())) {
            ResourceService.getCategoryFood({
                str: input,
                utente: user,
                location: UserService.getRmCn()
            }).then(
                function (data) {
                    $scope.optionsCat = data;
                },
                function (err) {
                    $scope.optionsCat = [];
                });
        }
        else {
            ResourceService.getCategory({
                str: input,
                utente: user
            }).then(
                function (data) {
                    $scope.optionsCat = data;
                },
                function (err) {
                    $scope.optionsCat = [];
                });
        }


        if (input.length < 1) {
            $scope.optionsCat = [];
            return [];
        }
        if (input.length === 1) {
            $scope.optionsProd = [];
            return ResourceService.getCategory({
                str: input,
                utente: user
            }).then(
                function (data) {
                    $scope.optionsCat = data;
                },
                function (err) {
                    $scope.optionsCat = [];
                });
        }
    };

    /*Funzione che crea dinamicamente la lista dei SP*/
    $scope.refreshSalePoint = function (input) {
        if (!angular.isDefined(input) || input === null) {
            $scope.seldisable[0] = false;
            return [];
        }

        if (input.length === 0) {
            return ResourceService.getPos({
                str: input
            }).then(
                function (data) {
                    if (angular.equals(ServiceCalculate.getPage(), "Food")) {
                        $scope.optionSalePoint = [];

                        for (var i = 0; i < data.length; i++) {
                            if (!angular.equals(data[i].name, UserService.getMagCen()))
                                $scope.optionSalePoint.push(data[i]);
                        }
                    }
                    else
                        $scope.optionSalePoint = data;
                },
                function (err) {
                    $scope.optionSalePoint = [];
                });
        }
    };

    $scope.collectIndex = function () {
        return ResourceService.getIndex({
            category: "product"
        }).then(
            function (data) {
                $scope.optionIndex = data;
            },
            function (err) {
                $scope.optionIndex = [];
            });
    };

    /*Funzione che crea dinamicamente la lista degli indici*/
    $scope.refreshIndex = function (input) {
        if (!angular.isDefined(input) || input === null) return [];
        if (input.length === 0) {
            return $scope.collectIndex();
        }
    };

    $scope.refreshIndexCat = function (input) {
        if (!angular.isDefined(input) || input === null) return [];
        if (angular.equals(UserService.getUser(), UserService.getML()))
            $scope.refreshIndexFood(input);
        else if (angular.equals(UserService.getUser(), UserService.getMC()))
            $scope.refreshIndexPro(input);
        else {
            if ($scope.rolesel) {
                $scope.refreshIndexFood(input);
            }
            else {
                $scope.refreshIndexPro(input);
            }
        }
    };

    /*Funzione che crea dinamicamente la lista degli indici*/
    $scope.refreshIndexPro = function (input) {
        if (!angular.isDefined(input) || input === null) return [];
        if (input.length === 0) {
            ResourceService.getIndex({
                category: "product"
            }).then(
                function (data) {
                    $scope.optionIndex = data;
                },
                function (err) {
                    $scope.optionIndex = [];
                });
        }
    };

    /*Funzione che crea dinamicamente la lista degli indici*/
    $scope.refreshIndexFood = function (input) {
        if (!angular.isDefined(input) || input === null) return [];
        if (input.length === 0) {
            ResourceService.getIndex({
                category: "food"
            }).then(
                function (data) {
                    $scope.optionIndex = data;
                },
                function (err) {
                    $scope.optionIndex = [];
                });
        }
    };

    /*Funzione che crea dinamicamente la lista dei Prodotti e delle categorie*/
    $scope.refreshProdCat = function (input) {
        if (!angular.isDefined(input) || input === null) {
            $scope.optionsProdCat = [];
            return [];
        }
        if (input.length < 1) {
            $scope.optionsProdCat = [];
            return [];
        }
        if (input.length === 1) {
            ResourceService.getCategory({
                str: input,
                utente: UserService.getUser()
            }).then(
                function (data) {
                    data = data.map(function (obj) {
                        return angular.extend(obj, {'type': 'cat'});
                    });
                    $scope.optionsProdCat = data;
                },
                function (err) {
                    $scope.optionsProdCat = [];
                });
        }
        if (input.length === 2) {
            ResourceService.getAllProduct({
                fourchar: input
            }).then(
                function (data) {
                    data = data.map(function (obj) {

                        return angular.extend(obj, {'type': 'prod'});
                    });
                    $scope.optionsProdCat = $scope.optionsProdCat.concat(data);
                },
                function (err) {
                    $scope.optionsProdCat = [];
                });
        }
    };

    $scope.selecVal = function (index) {
        ServiceCalculate.changeCmpIndex(index, $scope.optionIndex[index]);
    };


});