// CONTROLLER INDEX ECONOMICI
app.controller('ecoIndex', function ($scope, ServiceCalculate,ResourceService) {

    $scope.identityEconomicSP = 0;
    $scope.economicPS = [];
    $scope.optionSalePoint = [];


    $scope.documOn = function (value) {
        if (angular.equals(value, "Economic")) {
            ServiceCalculate.setPage("Economic");
        }
    };

    /*Funzione che aggiunge un elemento indice economico con id */
    $scope.addNewSalePoint = function (){
        if ($scope.economicPS.length < 4) {
            $scope.identityEconomicSP++;
            $scope.economicPS.push({'id': 'ecoPS' + $scope.identityEconomicSP});
        }
    };

    /*Funzione che rimuove all'array degli indici economici l'elemento value*/
    $scope.removeSalepoint = function (value) {
        ServiceCalculate.removeSalepoin(value + 1);
        $scope.economicPS.splice(value, 1);
    };

    /*Funzione che si occupa della selezione di un SP*/
    $scope.selectedSP = function (item, model, id) {
        ServiceCalculate.addSalePoint(item, id + 1);
    };

    /*Funzione che si occupa della selezione di un index*/
    $scope.selectedIndex = function (item) {
        ServiceCalculate.setIndex(item);
    };

    /*Funzione che crea dinamicamente la lista dei punti vendita*/
    $scope.refreshSalePoint = function (input) {
        if (!angular.isDefined(input) || input === null) {
            $scope.optionSalePoint = [];
            return [];
        }
        if (input.length === 0) {
            return ResourceService.getPos({
                str: ""
            }).then(
                function (data) {
                    $scope.optionSalePoint = data;
                },
                function () {
                    $scope.optionSalePoint = [];
                });
        }
    };

    /*Funzione che crea dinamicamente la lista degli indici*/
    $scope.refreshIndex = function (input) {
        if (!angular.isDefined(input) || input === null) return [];
        if (input.length === 0) {
            ResourceService.getIndex({
                category: "economic"
            }).then(
                function (data) {
                    $scope.optionIndex = data;
                },
                function () {
                    $scope.optionIndex = [];
                });
        }
    };


});