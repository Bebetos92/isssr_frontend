var app = angular.module("myApp", ["ngRoute", "ui.select2", '720kb.datepicker', 'ui.select', 'ngSanitize']);



app.config(function ($routeProvider) {

    $routeProvider
        .when("/", {
            templateUrl: "Views/viewHome/main.html"
        })
        .when("/indexProduct", {
            templateUrl: "Views/viewIndex/indexProduct.html"
        })
        .when("/indexCategory", {
            templateUrl: "Views/viewIndex/indexCategory.html"
        })
        .when("/indexCompare", {
            templateUrl: "Views/viewIndex/indexCompare.html"
        })
        .when("/indexEconomic", {
            templateUrl: "Views/viewEconomics/indexEconomic.html"
        })
        .when("/addNewCategory", {
            templateUrl: "Views/viewManageCategory/addNewCategory.html"
        })
        .when("/newIndex", {
            templateUrl: "Views/viewNewIndex/newIndex.html"
        })
        .when("/manageCategory", {
            templateUrl: "Views/viewManageCategory/manageCategory.html"
        })
        .when("/indexFood", {
            templateUrl: "Views/viewIndex/indexFood.html"
        })
        .when("/reports", {
            templateUrl: "Views/viewReports/menageReports.html"
        })

});