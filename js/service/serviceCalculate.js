/*
 *
 * Servizio per salvare i dati prima di lanciare il calcolo del grafico
 *
 */



app.service("ServiceCalculate", function ($rootScope, UserService, ResourceService) {

    var page = "";
    var dateStart = 0;
    var dateEnd = 0;
    var productSel = [];
    var categorySel = [];
    var salePointSel = [];
    var economicIndex = [];
    var compareIndex = [];
    var indexVal = 0;
    var user = "";
    var intervall = 0;


    this.setPage = function (value) {
        page = value;
        dateStart = 0;
        dateEnd = 0;
        productSel = [];
        categorySel = [];
        economicPS = [];
        salePointSel = [];
        compareIndex = [];
        indexVal = 0;
        user = "";
        intervall = 0;
    };

    this.getPage = function () {
        return page
    };

    this.setInterval = function (params) {
        intervall = params;
    };

    this.setDateStart = function (params) {
        dateStart = params;
    };

    this.setDateEnd = function (params) {
        dateEnd = params;
    };

    this.setIndex = function (value) {
        indexVal = value;
    };

    this.changeCmpIndex = function (value, index) {

        if (compareIndex[value] === null || compareIndex[value] === false || compareIndex[value] === undefined) {
            compareIndex[value] = index;

        }
        else {
            compareIndex[value] = false;
        }
    };

    this.addProduct = function (value, id) {
        productSel[id] = value;
    };

    this.addCategory = function (value, id) {
        categorySel[id] = value;
    };

    this.addSalePoint = function (value, id) {
        salePointSel[id] = value;
    };

    this.addEconomicIndex = function (value, id) {
        economicPS[id] = value;
    };

    this.removeIndex = function () {
        indexVal = null;
    };

    this.removeProduc = function (id) {
        productSel.splice(id, 1);
    };

    this.removeCategor = function (id) {
        categorySel.splice(id, 1);
    };

    this.removeSalepoin = function (id) {
        salePointSel[id] = null;
    };

    this.removeEconomicIndex = function (id) {
        economicPS[id] = null;
    };

    this.calculate = function () {
        var prod = [];
        var cate = [];
        var sp = [];
        var ind = [];
        var i;
        var param;
        user = UserService.getUser();

        if (dateStart === 0 || dateStart === null || dateStart === undefined) {
            alert("INSERIRE UNA DATA DI INIZIO");
            return;
        }

        if (dateEnd === 0 || dateEnd === null || dateEnd === undefined) {
            alert("INSERIRE UNA DATA DI FINE");
            return;
        }

        if (intervall === 0 || intervall === null || intervall === undefined) {
            alert("INSERIRE UN INTERVALLO");
            return;
        }


        switch (page) {
            case "Prod":
                if (indexVal === 0 || indexVal === null || indexVal === undefined) {
                    alert("SELEZIONARE UN INDICE");
                    return;
                }
                for (i = 0; i < productSel.length; i++) {
                    if (productSel[i] !== null && productSel[i] !== undefined) {
                        if (prod.indexOf(productSel[i].description) === -1)
                            prod.push(productSel[i].description);
                    }
                }
                if (angular.equals(prod, []) || prod === null || prod === undefined) {

                    alert("SELEZIONARE ALMENO UN PRODOTTO");
                    return;

                }

                switch (user) {
                    case UserService.getTM():
                        if (salePointSel[0] === null || salePointSel[0] === undefined) {
                            alert("Selezionare una locazione");
                            return;
                        }

                        ResourceService.computeIndexProduct({
                            start: dateStart,
                            end: dateEnd,
                            granularity: intervall,
                            descProduct: prod,
                            index: indexVal.nameIndex,
                            location: [salePointSel[0].name],
                            utente: user,
                            flag: ""

                        }).then(
                            function (data) {
                                $rootScope.dataGraph = data;
                                return data;
                            },
                            function (err) {
                                alert("Errore nel calcolo dell'indice");
                            });
                        break;

                    case UserService.getMC():
                        ResourceService.computeIndexProduct({
                            start: dateStart,
                            end: dateEnd,
                            granularity: intervall,
                            descProduct: prod,
                            index: indexVal.nameIndex,
                            location: [UserService.getMagCen()],
                            utente: user,
                            flag: ""
                        }).then(
                            function (data) {
                                $rootScope.dataGraph = data;
                                return data;
                            },
                            function (err) {
                                alert("Errore nel calcolo dell'indice");
                            });
                        break;

                    case UserService.getML():
                        ResourceService.computeIndexProduct({
                            start: dateStart,
                            end: dateEnd,
                            granularity: intervall,
                            descProduct: prod,
                            index: indexVal.nameIndex,
                            location: [UserService.getRmCn()],
                            utente: user,
                            flag: ""
                        }).then(
                            function (data) {
                                $rootScope.dataGraph = data;
                                return data;
                            },
                            function (err) {
                                alert("Errore nel calcolo dell'indice");
                            });
                        break;
                }
                break;
            case "Food":

                if (indexVal === 0 || indexVal === null || indexVal === undefined) {
                    alert("SELEZIONARE UN INDICE");
                    return;
                }
                for (i = 0; i < productSel.length; i++) {
                    if (productSel[i] !== null && productSel[i] !== undefined) {
                        if (prod.indexOf(productSel[i].nome) === -1)
                            prod.push(productSel[i].nome);
                    }
                }
                if (angular.equals(prod, []) || prod === null || prod === undefined) {

                    alert("SELEZIONARE ALMENO UN PRODOTTO");
                    return;

                }
                switch (user) {
                    case UserService.getTM():
                        ResourceService.computeIndexProduct({
                            start: dateStart,
                            end: dateEnd,
                            granularity: intervall,
                            descProduct: prod,
                            index: indexVal.nameIndex,
                            location: [salePointSel[0].name],
                            utente: user,
                            flag: ""
                        }).then(
                            function (data) {
                                $rootScope.dataGraph = data;
                                return data;
                            },
                            function (err) {
                                alert("Errore nel calcolo dell'indice");
                            });
                        break;
                    case UserService.getMC():
                        alert("Non si dispone dei diritti necessari");
                        break;
                    case UserService.getML():
                        ResourceService.computeIndexProduct({
                            start: dateStart,
                            end: dateEnd,
                            granularity: intervall,
                            descProduct: prod,
                            index: indexVal.nameIndex,
                            location: [UserService.getRmCn()],
                            utente: user,
                            flag: ""
                        }).then(
                            function (data) {
                                $rootScope.dataGraph = data;
                                return data;
                            },
                            function (err) {
                                alert("Errore nel calcolo dell'indice");
                            });
                        break;


                }
                break;


            case "Cat":
                if (indexVal === 0 || indexVal === null || indexVal === undefined) {
                    alert("SELEZIONARE UN INDICE");
                    return;
                }

                for (i = 0; i < categorySel.length; i++) {
                    if (categorySel[i] !== null && categorySel[i] !== undefined) {
                        if (cate.indexOf(categorySel[i].name) === -1)
                            cate.push(categorySel[i].name);
                    }
                }
                if (angular.equals(cate, []) || cate === null || cate === undefined) {
                    alert("SELEZIONARE ALMENO UNA CATEGORIA");
                    return;
                }
                switch (user) {
                    case UserService.getTM():

                        ResourceService.computeIndexProduct({
                            start: dateStart,
                            end: dateEnd,
                            granularity: intervall,
                            descProduct: cate,
                            index: indexVal.nameIndex,
                            location: [UserService.getRmCn()],
                            flag: "Category",
                            utente: user
                        }).then(
                            function (data) {
                                $rootScope.dataGraph = data;
                                return data;
                            },
                            function (err) {
                                alert("Errore nel calcolo dell'indice");
                            });

                        break;
                    case UserService.getMC():

                        ResourceService.computeIndexProduct({
                            start: dateStart,
                            end: dateEnd,
                            granularity: intervall,
                            descProduct: cate,
                            index: indexVal.nameIndex,
                            location: [UserService.getMagCen()],
                            flag: "Category",
                            utente: user
                        }).then(
                            function (data) {
                                $rootScope.dataGraph = data;
                                return data;
                            },
                            function (err) {
                                alert("Errore nel calcolo dell'indice");
                            });

                        break;
                    case UserService.getML():

                        ResourceService.computeIndexProduct({
                            start: dateStart,
                            end: dateEnd,
                            granularity: intervall,
                            descProduct: cate,
                            index: indexVal.nameIndex,
                            location: [UserService.getRmCn()],
                            flag: "Category",
                            utente: user
                        }).then(
                            function (data) {
                                $rootScope.dataGraph = data;
                                return data;
                            },
                            function (err) {
                                alert("Errore nel calcolo dell'indice");
                            });

                        break;
                }
                break;

            case "Economic":
                if (indexVal === 0 || indexVal === null || indexVal === undefined) {
                    alert("SELEZIONARE UN INDICE");
                    return;
                }

                switch (user) {
                    case UserService.getTM():
                        for (i = 0; i < salePointSel.length; i++) {
                            if (salePointSel[i] !== null && salePointSel[i] !== undefined) {
                                if (sp.indexOf(salePointSel[i].name) === -1)
                                    sp.push(salePointSel[i].name);
                            }
                        }
                        if (angular.equals(sp, []) || sp === null || sp === undefined) {
                            alert("SELEZIONARE ALMENO UN PUNTO VENDITA");
                            return;
                        }
                        ResourceService.computeIndexProduct({
                            start: dateStart,
                            end: dateEnd,
                            granularity: intervall,
                            descProduct: [],
                            index: indexVal.nameIndex,
                            location: sp,
                            flag: "",
                            utente: user
                        }).then(
                            function (data) {
                                $rootScope.dataGraph = data;
                                return data;
                            },
                            function (err) {
                                alert("Errore nel calcolo dell'indice");
                            });
                        
                        break;
                    case UserService.getMC():
                        ResourceService.computeIndexProduct({
                            start: dateStart,
                            end: dateEnd,
                            granularity: intervall,
                            descProduct: [],
                            index: indexVal.nameIndex,
                            location: [UserService.getMagCen()],
                            flag: "",
                            utente: user
                        }).then(
                            function (data) {
                                $rootScope.dataGraph = data;
                                return data;
                            },
                            function (err) {
                                alert("Errore nel calcolo dell'indice");
                            });

                        break;
                    case UserService.getML():

                        ResourceService.computeIndexProduct({
                            start: dateStart,
                            end: dateEnd,
                            granularity: intervall,
                            descProduct: [],
                            index: indexVal.nameIndex,
                            location: [UserService.getRmCn()],
                            flag: "",
                            utente: user
                        }).then(
                            function (data) {
                                $rootScope.dataGraph = data;
                                return data;
                            },
                            function (err) {
                                alert("Errore nel calcolo dell'indice");
                            });


                        break;
                }
                break;

            case "Comp":
                for (i = 0; i < salePointSel.length; i++) {
                    if (salePointSel[i] !== null && salePointSel[i] !== undefined) {
                        alert(salePointSel[i].name);
                        if (sp.indexOf(salePointSel[i].name) === -1)
                            sp.push(salePointSel[i].name);
                    }
                }
                if (angular.equals(sp, []) || sp === null || sp === undefined) {
                    alert("SELEZIONARE ALMENO UN PUNTO VENDITA");
                    return;
                }
                for (i = 0; i < categorySel.length; i++) {
                    if (categorySel[i] !== null && categorySel[i] !== undefined) {
                        alert(categorySel[i].description);
                        if (cate.indexOf(categorySel[i].description) === -1)
                            cate.push(categorySel[i].description);
                    }
                }
                for (i = 0; i < productSel.length; i++) {
                    if (productSel[i] !== null && productSel[i] !== undefined) {
                        alert(productSel[i].description);
                        if (prod.indexOf(productSel[i].description) === -1)
                            prod.push(productSel[i].description);
                    }
                }
                if ((angular.equals(prod, []) && angular.equals(cate, [])) || prod === null || prod === undefined || cate === null || cate === undefined) {

                    alert("SELEZIONARE ALMENO UN PRODOTTO O UNA CATEGORIA");
                    return;

                }
                for (i = 0; i < compareIndex.length; i++) {
                    if (compareIndex[i] !== false && compareIndex[i] !== null && compareIndex[i] !== undefined) {
                        alert(compareIndex[i].nameIndex);
                        ind.push(compareIndex[i].nameIndex);
                    }
                }
                if (angular.equals(ind, []) || ind === null || ind === undefined) {
                    alert("SELEZIONARE ALMENO UN INDICE");
                    return;
                }
                switch (user) {
                    case UserService.getTM():
                        break;
                    case UserService.getMC():
                        break;
                    case UserService.getML():
                        alert("Non si disponde dei permessi necessari");
                        break;
                }
                break;
        }
    };

})
;