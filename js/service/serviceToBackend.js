app.service("ResourceService", function ($q, $http) {


    var ajax = function (method, url, data) {

        var deferred = $q.defer();
        var request = {
            method: method,
            url: url,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        if (method === 'GET' || method === 'DELETE') {
            request.params = data;
        } else {
            request.data = data;
        }
        $http(request).then(function (response) {
            deferred.resolve(response.data);
        }, function (response, status) {
            deferred.reject({data: response, status: status});
        });
        return deferred.promise;
    };


    this.getAllProduct = function (params) {
        return ajax("GET", "http://localhost:8080/product/findProdottiByString", params);
    };
    this.getPartialProducts = function (params) {
        return ajax("GET", "http://localhost:8080/cat/riempiProduct", params);
    };
    this.getCategory = function (params) {
        return ajax("GET", "http://localhost:8080/cat/riempiCat", params);
    };
    this.getPos = function (params) {
        return ajax("GET", "http://localhost:8080/pos/findAll", params);
    };
    this.addCat = function (params) {
        return ajax("POST", "http://localhost:8080/cat/addNewCat", params);
    };
    this.addCatFood = function (params) {
        return ajax("POST", "http://localhost:8080/catFood/addNewCatFood", params);
    };
    this.deleteCategory = function (params) {
        return ajax("DELETE", "http://localhost:8080/cat/removeCat", params);
    };
    this.updateDB = function (params) {
        return ajax("GET", "http://localhost:8080/util/updateAll", params)
    };

    this.getReports = function (params) {
        return ajax("GET", "http://localhost:8080/report/getAll", params);
    };
    this.deleteReport = function (param) {
        return ajax("DELETE", "http://localhost:8080/report/"+param.utente+"/deleteReport="+param.name, "");
    };
    this.getVariable = function (param) {
        return ajax("GET", "http://localhost:8080/index/getVariables=" + param.str);
    };

    this.saveIndex = function (param) {
        return ajax("POST","http://localhost:8080/index/addIndex",param);
    };
    this.getPartialFood = function (params) {
        return ajax("GET", "http://localhost:8080/food/findFoodLocation", params);
    };

    this.getCategoryFood = function (param) {
      return ajax("GET","http://localhost:8080/catFood/fillCatIndex",param);
    };

    this.deleteCategoryFood = function (param) {
      return ajax("DELETE","http://localhost:8080/catFood/removeCatFood",param);
    };
    this.getFoodCategory = function (param) {
      return ajax("GET","http://localhost:8080/catFood/getFoodByCatLoc",param);
    };


    // Richieste dei risultati
    this.computeIndexProduct = function (params) {
        return ajax("POST", "http://localhost:8080/index/getReport", params);

     };

    this.getIndex = function (params) {
        return ajax("GET", "http://localhost:8080/index/getIndexBySemantic", params);

    };

    this.getIndexFood = function (params) {
        //return ajax("GET", "http://localhost:8080/index/findAllFood", params); //TODO
    };

    this.loadG = function () {
        return ajax("GET", "http://localhost:8080/report/getReport=3", "");
    };

    this.getTopThreeProduct = function (params) {
        return ajax("GET", "http://localhost:8080/main/getTopProduct", params);
    };

    this.getTopThreeProduct2 = function (params) {
        return ajax("GET", "http://localhost:8080/main/getTopProduct", params);
    };

    this.getTopThreeFood = function (params) {
        return ajax("GET", "http://localhost:8080/main/getTopFood", params);
    };


    this.getLastReports = function (params) {
        return ajax("GET", "http://localhost:8080/report/getLast", params);
    }

});